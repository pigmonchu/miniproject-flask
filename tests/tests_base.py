from the_app import create_app
from flask_testing import TestCase


class TestBase(TestCase):
    def create_app(self):
        app = create_app()
        return app

    def setUp(self):
        self.client = self.app.test_client()
        self.client.testing = True

    def tearDown(self):
        pass
