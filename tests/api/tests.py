from tests.tests_base import TestBase
from flask import url_for
import json


class TestAPI(TestBase):
    def test_return_API(self):
        response = self.client.get(url_for('api.api_example', id=1))
        self.assert200
        jsondata = json.loads(response.data)
        self.assertEqual(jsondata.get('dato'), "1")
