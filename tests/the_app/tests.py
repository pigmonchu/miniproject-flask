from tests.tests_base import TestBase

from flask import url_for


class TestIndex(TestBase):
    def test_server_on(self):
        response = self.client.get(url_for('index'))
        self.assert200
        self.assertEqual(response.data, b"Hola, mundos!")

    def test_another_route(self):
        response = self.client.get(url_for('another'))
        self.assert200
        self.assertEqual(response.data, b"Soy otro endpoint 13")
