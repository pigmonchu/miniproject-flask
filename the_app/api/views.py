from flask import make_response, jsonify
from flask.views import MethodView
from http import HTTPStatus


class API(MethodView):
    def get(self, id):
        return make_response(jsonify({"dato": id})), HTTPStatus.OK
