from the_app.api.views import API
from . import api

api_view = API.as_view('api_example')
api.add_url_rule(
    '/api/v1.0/example/<id>',
    view_func=api_view,
    methods=['GET']
)
