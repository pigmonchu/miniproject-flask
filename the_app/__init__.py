from flask import Flask
from .api import api as api_blueprint


def create_app():
    app = Flask(__name__)

    @app.route("/")
    def index():
        return "Hola, mundos!"

    @app.route("/another")
    def another():
        return "Soy otro endpoint 13"

    app.register_blueprint(api_blueprint)
    return app
