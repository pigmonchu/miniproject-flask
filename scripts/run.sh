#!/bin/bash

RUN_API=${RUN_API:='yes'}
WSGI_HOST=${WSGI_HOST:='0.0.0.0'}
WSGI_PORT=${WSGI_PORT:=8000}
WSGI_WORKERS=${WSGI_WORKERS:=1}
WSGI_TIMEOUT=${WSGI_TIMEOUT:=120}

APPLY_MIGRATIONS=${APPLY_MIGRATIONS:='yes'}

# API
if [ $RUN_API = "yes" ]; then
cat >/code/circus.ini <<EOF
[watcher:api]
working_dir = /code
cmd = gunicorn
args = -w ${WSGI_WORKERS} -t ${WSGI_TIMEOUT} --pythonpath=. -b ${WSGI_HOST}:${WSGI_PORT} run:app
uid = root
numprocesses = 1
autostart = true
send_hup = true
copy_env = True

EOF
fi

echo "Start circus" && circusd /code/circus.ini