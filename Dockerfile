# python v 3.8
FROM python:3.8-buster
ENV PYTHONBUFFERED 1

# including code the_app
RUN mkdir /code
WORKDIR /code
COPY . /code/

# Dependencies
RUN \
    apt-get update                      && \
    apt-get install -y build-essential  && \
    pip install --upgrade pip           && \
    pip install gunicorn                && \
    pip install circus chaussette       && \
    pip install -r requirements.txt     && \
    apt-get purge -y build-essential    && \
    apt-get autoremove -y               

EXPOSE 8000


#ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:8000", "run:app"]
#CMD gunicorn -b 0.0.0.0:8000 run:app

# Runs the apps
ENTRYPOINT [ "bash", "/code/scripts/run.sh" ] 
